#! /usr/bin/python

from gimpfu import *
import gtk
import gimpui
import gobject

###################
### Definitions ###
###################

window = None
window_box = None
animations_vbox = None

# global state
spritesheet = None
spritesheet_width = -1
spritesheet_height = -1
frame_width = -1
frame_height = -1
animations = {}
dirty = False

created = False

clock = None
time = 0.1
x = -1

def frames_changed(anim):
    anim.valid_frames = []
    for f in anim.frames:
        if f.x >= 0 and f.y >= 0:
            anim.valid_frames.append(f)

def entry_x_changed(widget, args):
    frame = args[0]
    anim = args[1]
    text = widget.get_text()
    if text.isdigit():
        frame.x = int(text)
        frames_changed(anim)

def entry_y_changed(widget, args):
    frame = args[0]
    anim = args[1]
    text = widget.get_text()
    if text.isdigit():
        frame.y = int(text)
        frames_changed(anim)

def add_frame(widget, anim):
    f = Frame()
    f.vbox = gtk.VBox()
    entry_x = gtk.Entry()
    entry_x.set_text("-1")
    entry_x.connect("changed", entry_x_changed, [f, anim])
    f.vbox.pack_start(entry_x, True, True, 0)
    entry_y = gtk.Entry()
    entry_y.set_text("-1")
    entry_y.connect("changed", entry_y_changed, [f, anim])
    f.vbox.pack_start(entry_y, True, True, 0)
    b = gtk.Button()
    b.set_label("Delete")
    b.connect("clicked", delete_frame, [anim, f])
    f.vbox.pack_start(b, True, True, 0)

    f.pos = len(anim.frames)
    anim.frames.append(f)
    anim.frames_box.pack_start(f.vbox, True, True, 0)
    
    f.vbox.show_all()

    frames_changed(anim)

def delete_frame(widget, args):
    anim = args[0]
    frame = args[1]
    
    for i in range(frame.pos, len(anim.frames)):
        anim.frames[i].pos = i - 1

    anim.frames.pop(frame.pos + 1)
    anim.frames_box.remove(frame.vbox)

    frames_changed(anim)
        

class Frame:
    x = -1
    y = -1
    vbox = None
    pos = -1

class Animation:
    vbox = None
    drawable_preview = None
    copied_image = None
    frame_layer = None
    merged_layer = None
    temp_layer = None
    name = ""
    time = 2.0 # seconds in float
    step = 0.1
    frames = []
    valid_frames = []
    clock = None
    posx = -1
    posy = -1
    stop_animation = False
    playing_animation = False
    dirty = False # whether image was changed and animation needs updating
    pos = None

    def recreate():
        return

def save():
    return

def load():
    return

def add_animation(anim):
    if anim.name in animations:
        return False
    animations[anim.name] = anim
    return True

def remove_animation(anim):
    del animations[anim.name]

def next_frame(anim):
    if anim.stop_animation:
        anim.stop_animation = False
        anim.playing_animation = False
        anim.pos = None
        return
    
    frames = anim.valid_frames
    num = len(frames)
    
    if num == 0:
        anim.playing_animation = False
        anim.pos = None
        return

    anim.pos = anim.pos + 1

    if anim.pos == num:
        anim.pos = 0
    show_frame(anim, frames[anim.pos].x, frames[anim.pos].y)
    gobject.timeout_add(int(time * 1000), next_frame, anim)

def play_animation(widget, anim):
    if anim.playing_animation:
        return
    anim.pos = 0
    anim.playing_animation = True
    next_frame(anim)

def stop_animation(widget, anim):
    anim.stop_animation = True

def create_window():
    global window
    global window_box
    window = gtk.Window()
    window.set_title("Animation preview")
    window.connect('destroy',  close_animation_preview_window)
    window_box = gtk.VBox()
    window.add(window_box)

    button_box = gtk.HBox()
    window_box.pack_start(button_box, True, True, 0)

    create_button = gtk.Button()
    create_button.set_label("Create new animation")
    create_button.connect("clicked", create_button_clicked)
    button_box.pack_start(create_button, True, True, 0)

    name_entry = gtk.Entry()
    name_entry.set_text("Name")
    button_box.pack_start(name_entry, True, True, 0)

    width_entry = gtk.Entry()
    width_entry.set_text("write the frame's width")
    button_box.pack_start(width_entry, True, True, 0)

    height_entry = gtk.Entry()
    height_entry.set_text("write the frame's height")
    button_box.pack_start(height_entry, True, True, 0)

    global animations_vbox
    animations_vbox = gtk.VBox()
    window_box.pack_start(animations_vbox, True, True, 0)

    window.show_all()

def create_button_clicked(widget):
    global created
    if created:
        return
    created = True
    a = Animation()
    a.name = "first"

    animations[a.name] = a
    
    # make a copy of the spritesheet and merge the layers
    a.copied_image = pdb.gimp_image_duplicate(spritesheet)
    pdb.gimp_image_merge_visible_layers(a.copied_image, 0)
    a.merged_layer = a.copied_image.layers[0] # TODO try active_layer
    pdb.gimp_drawable_set_name(a.merged_layer, "merged_layer")

    # create a new layer at the size of the frame and clear it
    # this is where we paste the frame and show it while animating
    # slow I know
    a.frame_layer = gimp.Layer.copy(a.merged_layer) # TODO why not just create new, maybe faster
    a.copied_image.add_layer(a.frame_layer)
    a.frame_layer.resize(frame_width, frame_height)
    pdb.gimp_drawable_set_name(a.frame_layer, "frame_layer")
    pdb.gimp_drawable_edit_clear(a.frame_layer)

    # create a temp layer at the size of the spritesheet and clear it
    # temp layer is used to paste the frame we copied from merged layer and offset it to the position 0, 0
    # then we copy it again and paste it to the smaller frame layer
    # right, slow...
    a.temp_layer = gimp.Layer.copy(a.merged_layer)
    a.copied_image.add_layer(a.temp_layer)
    pdb.gimp_drawable_set_name(a.temp_layer, "temp_layer")
    pdb.gimp_drawable_edit_clear(a.temp_layer)

    # create a vertical box that contains all widgets for our new animation and add it to the animations vbox
    global animations_vbox
    a.vbox = gtk.VBox()
    animations_vbox.pack_start(a.vbox, True, True, 0)

    a.frames_box = gtk.HBox()
    a.vbox.pack_start(a.frames_box, True, True, 0)

    a.drawable_preview = gimpui.DrawablePreview(a.frame_layer)
    a.frames_box.pack_start(a.drawable_preview, True, True, 0)

    animation_buttons_box = gtk.HBox()
    a.vbox.pack_start(animation_buttons_box, True, True, 0)

    play_button = gtk.Button()
    play_button.set_label("Play")
    play_button.connect("clicked", play_animation, a)
    animation_buttons_box.pack_start(play_button, True, True, 0)

    stop_button = gtk.Button()
    stop_button.set_label("Stop")
    stop_button.connect("clicked", stop_animation, a)
    animation_buttons_box.pack_start(stop_button, True, True, 0)

    add_frame_button = gtk.Button()
    add_frame_button.set_label("Add frame")
    add_frame_button.connect("clicked", add_frame, a)
    animation_buttons_box.pack_start(add_frame_button, True, True, 0)

    show_frame(a, 0, 0)

    window.show_all()

"""
def old_create_button_clicked(widget):
    global created
    if created:
        return
    created = True

    global copied_image
    copied_image = pdb.gimp_image_duplicate(spritesheet)
    pdb.gimp_image_merge_visible_layers(copied_image, 0)
    global merged_layer
    merged_layer = copied_image.layers[0]
    pdb.gimp_drawable_set_name(merged_layer, "merged_layer")

    global frame_layer
    frame_layer = gimp.Layer.copy(merged_layer)
    copied_image.add_layer(frame_layer)
    frame_layer.resize(frame_width, frame_height)
    pdb.gimp_drawable_set_name(frame_layer, "frame_layer")

    # temp layer is used to paste the frame and offset it before adding it to the smaller frame layer
    global temp_layer
    temp_layer = gimp.Layer.copy(merged_layer)
    copied_image.add_layer(temp_layer)
    pdb.gimp_drawable_set_name(temp_layer, "temp_layer")
    pdb.gimp_drawable_edit_clear(temp_layer)

    global animations_vbox
    anim_vbox = gtk.VBox()
    animations_vbox.pack_start(anim_vbox, True, True, 0)

    global drawable_preview
    drawable_preview = gimpui.DrawablePreview(frame_layer)
    anim_vbox.pack_start(drawable_preview, True, True, 0)

    animation_buttons_box = gtk.HBox()
    anim_vbox.pack_start(animation_buttons_box, True, True, 0)

    play_button = gtk.Button()
    play_button.set_label("Play")
    play_button.connect("clicked", play_animation)
    animation_buttons_box.pack_start(play_button, True, True, 0)

    stop_button = gtk.Button()
    stop_button.set_label("Stop")
    stop_button.connect("clicked", stop_animation)
    animation_buttons_box.pack_start(stop_button, True, True, 0)

    show_frame(0, 0)
    window.show_all()
"""

def show_frame(a, x, y):
    a.x = x
    a.y = y
    
    real_x = x * frame_width
    real_y = y * frame_height

    # clear frame_layer and temp_layer
    pdb.gimp_drawable_edit_clear(a.frame_layer)
    pdb.gimp_drawable_edit_clear(a.temp_layer)

    # select merged_layer
    pdb.gimp_image_set_active_layer(a.copied_image, a.merged_layer)
    # select frame rectangle
    pdb.gimp_image_select_rectangle(a.copied_image, 0, real_x, real_y, frame_width, frame_height) 
    # copy the frame we want
    pdb.gimp_edit_copy(a.merged_layer)

    # paste selection to temp layer
    s = pdb.gimp_edit_paste(a.temp_layer, False)
    # anchor it to its position
    pdb.gimp_floating_sel_anchor(s)
    # select again the frame now in the temp layer
    pdb.gimp_image_select_rectangle(a.copied_image, 0, real_x, real_y, frame_width, frame_height) 
    # cut the frame and offset it to the position 0,0
    s = pdb.gimp_selection_float(a.temp_layer, -real_x, -real_y)
    # anchor it to temp layer
    pdb.gimp_floating_sel_anchor(s)
    # select frame rectangle now in position 0,0
    pdb.gimp_image_select_rectangle(a.copied_image, 0, 0, 0, frame_width, frame_height) 
    # copy the frame we want
    pdb.gimp_edit_copy(a.temp_layer)

    #select frame_layer
    pdb.gimp_image_set_active_layer(a.copied_image, a.frame_layer) # maybe not needed to select it
    #paste selection
    s = pdb.gimp_edit_paste(a.frame_layer, False)
    pdb.gimp_floating_sel_anchor(s)

    a.drawable_preview.draw()

def close_animation_preview_window(ret):
    print("quitting Animation preview plugin")

    # Don't now why this is needed but gimp doesn't respond after closing the plugin if I don't do this - I suck at python
    for d in animations.values():
        d.drawable_preview = None
    clear_memory()

    gtk.main_quit()

def run_plugin_function(_image, _drawable):
    print("Running Animation Preview plugin")
    global spritesheet
    global spritesheet_width
    global spritesheet_height
    global frame_width
    global frame_height
    spritesheet = _image
    spritesheet_width = spritesheet.width
    spritesheet_height = spritesheet.height
    frame_width = 85
    frame_height = 128

    create_window()

    gtk.main()

    clear_memory()

def clear_memory():
    anims = animations.values()
    for a in anims:
        remove_animation(a)

######################
##### Run script #####
######################

register(
          "run_plugin_function",
          "blurb",
          "help message",
          "author",
          "BSD license",
          "10000",
          "Animation preview",
          "*",
          [
              (PF_IMAGE, "image", "Input image", None),
              (PF_DRAWABLE, "drawable", "Input drawable", None),
          ],
          [],
          run_plugin_function, menu="<Image>/Windows")
main()

